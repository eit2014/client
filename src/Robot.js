function Robot(transparent_robot){
    $.ajax({
        url: 'config/model.json',
    async: false,
    dataType: 'json',
    success: function(response) {
        robot_json = response;
    }
    });
    this.transparent_robot = transparent_robot;
}

function get_angle(acuatorId, part){
    if(part.name.localeCompare(acuatorId)==0) {
        return (acuatorsInParrents[part.name]?-1:1)*part.rotation.z*180/Math.PI;
    }
    for(var i=0;i<part.children.length;i++) {
        return get_angle(acuatorId, part.children[i]);
    }
}

function set_angle(angle, acuatorId, part){
    if(part.name.localeCompare(acuatorId)==0) {
        part.rotation.z = (acuatorsInParrents[part.name]?-1:1)*angle/180*Math.PI;
    }
    for(var i=0;i<part.children.length;i++) {
        set_angle(angle, acuatorId, part.children[i]);
    }
}

Robot.prototype.get_robot_model = function(){
    return this.build_robot_model();
}

Robot.prototype.build_robot_model = function() {
    var material 
        if(this.transparent_robot) {
            material = new THREE.MeshLambertMaterial( { color: 0x999999, transparent: true, opacity: 0.3, overdraw: true } );
        } else {
            material = new THREE.MeshLambertMaterial( { color: 0x999999, overdraw: true } );
        }
    this.robot_model = new THREE.Object3D();
    var local_robot = this.robot_model;
    this.robot_model.name = "root";
    loader.load('config/' + robot_json.modelId + '.js', function (geometry, materials) {
        var mesh = new THREE.Mesh(
            geometry,
            material
            );
        mesh.receiveShadow = true;
        mesh.castShadow = true;
        //mesh.rotation.y = -Math.PI/5;

        mesh.rotation.z = Math.PI/2;
        mesh.position.x = -15;
        local_robot.add(mesh);
    });
    this.robot_model.rotation.x = robot_json.modelAngleX/180*Math.PI;
    this.robot_model.rotation.y = robot_json.modelAngleY/180*Math.PI;
    this.robot_model.rotation.z = robot_json.modelAngleZ/180*Math.PI;
    this.robot_model.position.x = robot_json.modelOffsetX;
    this.robot_model.position.y = robot_json.modelOffsetY;
    this.robot_model.position.z = robot_json.modelOffsetZ;
    for(var i=0;i<robot_json.joints.length;i++) {
        this.robot_model.add(this.add_part(robot_json.joints[i]));
    }
    return this.robot_model;
}

Robot.prototype.add_part = function(part_json) {
    var robot_part_old = new THREE.Object3D();
    robot_part_old.name = part_json.acuatorId.toString();
    acuatorsInParrents[robot_part_old.name] = part_json.acuatorInParrent;
    robot_part_old.position.z = part_json.d;
    robot_part_old.rotation.z = part_json.theta/180*Math.PI;
    var robot_part_new = new THREE.Object3D();
    robot_part_new.position.x = part_json.a;
    robot_part_new.rotation.x = part_json.alpha/180*Math.PI;
    robot_part_old.add(robot_part_new);
    var material 
        if(this.transparent_robot) {
            material = new THREE.MeshLambertMaterial( { color: 0x999999, transparent: true, opacity: 0.2, overdraw: true } );
        } else {
            material = new THREE.MeshLambertMaterial( { color: 0x999999, overdraw: true } );
        }
    loader.load('config/' + part_json.modelId + '.js', function (geometry, materials) {
        var mesh = new THREE.Mesh(
            geometry,
            material
            );
        mesh.receiveShadow = true;
        mesh.castShadow = true;
        mesh.rotation.x = part_json.modelAngleX/180*Math.PI;
        mesh.rotation.y = part_json.modelAngleY/180*Math.PI;
        mesh.rotation.z = part_json.modelAngleZ/180*Math.PI;
        mesh.position.x = part_json.modelOffsetX;
        mesh.position.y = part_json.modelOffsetY;
        mesh.position.z = part_json.modelOffsetZ;

        robot_part_new.add(mesh);
    });

    for(var i=0;i<part_json.joints.length;i++) {
        robot_part_new.add(this.add_part(part_json.joints[i]));
    }
    return robot_part_old;
}
