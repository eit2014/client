var command_socket = new Ws();
var stream_socket = new Ws();

var ip = "78.91.50.110";

command_socket.connect("ws://" + ip + ":5555");
stream_socket.connect("ws://" + ip + ":1991");

stream_socket.receive_handler = stream_handler;
var command_rpc_client = new $.JsonRpcClient({
    getSocket: function (onmessage_cb) {
        command_socket.receive_handler = onmessage_cb;
        return command_socket;
    }
});

function result_handler(result) {
	console.log('Got RPC call response: ' + result);
}

function error_handler(error)  {
	console.log('Got RPC call error: ');
	console.log(error);
}

function setAngle(angle, id) {
	command_rpc_client.call(
			'robot.setAngle',
			[ angle, id ],
			result_handler,
			error_handler
			);
	console.log(angle+ ' ' + id)
}

function setRobotPosition(x,y,z) {
    command_rpc_client.call(
            'robot.setPosition',
            [ [x , y , z] ],
            result_handler,
            error_handler
            );
}

// angles er vinkler til servoene, [0.2, 0.4, 0.5]
// id er ider til aktuelle servoer [4 2 3]
function setAllAngles(angles, id) {
    command_rpc_client.call(
            'robot.setAllAngles',
            [angles,id],
            result_handler,
            error_handler
            );
}

