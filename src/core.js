var camera, scene, renderer, loader, mesh, real_robot_global, acuatorsInParrents;
var cubes = [];
var numberOfCubes = 120;
var camera_distance = 225;
var robotCanvas;
var slider1, slider2, slider3;
var canvasBox;
var light, light1, light2;

function init() {
    robotCanvas = document.getElementById("robotCanvas");
    canvasBox = document.getElementById("canvasBox");
    robotCanvas.style.width = '100%';
    robotCanvas.style.height = '100%';
    robotCanvas.width = canvasBox.offsetWidth;
    robotCanvas.height = canvasBox.offsetHeight;

    renderer = new THREE.WebGLRenderer({canvas : robotCanvas});
    renderer.setSize( robotCanvas.width, robotCanvas.height );
    renderer.setClearColor( 0xeeeeee, 1);

    camera = new THREE.PerspectiveCamera( 75, robotCanvas.width / robotCanvas.height, 0.01, 2000 );
    camera.position.x = camera_distance;
    //camera.position.z = -camera_distance/5;
    camera.position.y = camera_distance/4;

    c = camera;

    scene = new THREE.Scene();

    initRobot();

    initGround();

    initFog();

    initLights();

    animate();
};

function createSliders() {

    slider1 = new dhtmlxSlider("sliderBox", {
        skin: "dhx_skyblue",
            min: -160,
            max: 160,
            step: 1,
            size: 300,
            value: 0,
            vertical: false
    });
    slider1.attachEvent("onChange",function(nv){document.getElementById("input1").value=nv;});
    slider1.attachEvent("onChange",function(){updateRobot();});	

    slider2 = new dhtmlxSlider("sliderBox", {
        skin: "dhx_skyblue",
            min: -5,
            max: 210,
            step: 1,
            size: 300,
            value: 0,
            vertical: false
    });
    slider2.attachEvent("onChange",function(nv){document.getElementById("input2").value=nv;});
    slider2.attachEvent("onChange",function(){updateRobot();});

    slider3 = new dhtmlxSlider("sliderBox", {
        skin: "dhx_skyblue",
            min: -105,
            max: 105,
            step: 1,
            size: 300,
            value: 0,
            vertical: false
    });
    slider3.attachEvent("onChange",function(nv){document.getElementById("input3").value=nv;});
    slider3.attachEvent("onChange",function(){updateRobot();});

    initSliders();
}

function initSliders() {
    slider1.init();
    slider2.init();
    slider3.init();
}

function updateRobot() {
    set_angle(slider1.getValue(), 1, this.control_robot.robot_model);
    set_angle(slider2.getValue(), 2, this.control_robot.robot_model);
    set_angle(slider3.getValue(), 3, this.control_robot.robot_model);
    setAngle(1,get_angle(1,this.control_robot.robot_model));
    setAngle(2,get_angle(2,this.control_robot.robot_model));
    setAngle(3,get_angle(3,this.control_robot.robot_model));
}


function sendPos(){
    var xInput=document.getElementById("xInput").value;
    var yInput=document.getElementById("yInput").value;
    var zInput=document.getElementById("zInput").value;

    setRobotPosition(xInput,yInput,zInput);
}

function animate() {
    var t = Date.now(); //the number of milliseconds elapsed since 1 January 1970 00:00:00 UTC
    requestAnimationFrame( animate );
    renderer.render( scene, camera );
    camera.lookAt(new THREE.Vector3(0,80,0));
};

function initRobot() {
    loader = new THREE.JSONLoader();
    acuatorsInParrents = {};
    this.real_robot = new Robot(false);
    this.control_robot = new Robot(true);

    real_robot_global = this.real_robot;
    control_robot_global = this.control_robot;

    scene.add(this.real_robot.get_robot_model());
    scene.add(this.control_robot.get_robot_model());
}

function initFog() {
    scene.fog = new THREE.FogExp2( 0xffffff, 0.0009 );
}

function initGround() {
    var planeW = 50; // pixels
    var planeH = 50; // pixels 
    var numW = 100; // how many wide (50*50 = 2500 pixels wide)
    var numH = 100; // how many tall (50*50 = 2500 pixels tall)
    var plane = new THREE.Mesh( new THREE.PlaneGeometry( planeW*50, planeH*50, planeW, planeH ), new   THREE.MeshBasicMaterial( { color: 0x000000, wireframe: true } ) );
    plane.rotation.x = Math.PI*0.5;
    plane.position.y = -80;
    scene.add(plane);
}

function initLights() {
    light = new THREE.PointLight( 0x113344, 15, 0 );
    light.position.set(100, 500, 0);
    scene.add(light);

    light1 = new THREE.PointLight( 0x004400, 15, 0 );
    light1.position.set(500, 100, 0);
    //scene.add(light1);

    light2 = new THREE.PointLight( 0x000044, 15, 0 );
    light2.position.set(0, 100, 500);
    //scene.add(light2);

    var ambientLight = new THREE.AmbientLight(0xbbbbbb);
    //    scene.add(ambientLight);

    // add subtle ambient lighting
    var ambientLight = new THREE.AmbientLight(0x111111);
    scene.add(ambientLight);

    // directional lighting
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 1, 1).normalize();
    //scene.add(directionalLight);
}

function stream_handler(d) {
    status_json = jQuery.parseJSON( d );
    for(var i=0;i<status_json.length;i++) {
        var status_angle = status_json[i].state.angle;
        var status_acuator_id = status_json[i].id;
        set_angle(status_angle, status_acuator_id, real_robot_global.robot_model);
    }
}
